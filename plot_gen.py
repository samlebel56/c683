import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.patches import PathPatch
import csv
import sys, getopt


def str_to_int(str, default = 0):
    if str.isdigit():
        return int(str)
    else:
        return default


def arg_to_file(name, ext, default):
    if len(name) == 0:
            return default
    elif 0 < len(name) < len(ext):
        return name + ext
    elif len(arg) >= len(ext) and arg[-len(ext):len(arg)] != ext:
        return arg + ext
    else:
        return name


def str_to_float(str, default = 0.0):
    try:
        res = float(str)
        return res
    except:
        return default


# OPTIONS
width = 7 # in
height = 5 # in
distance = 30.48 # cm # 30.48; 60.96; 91.44
incsv = 'data.csv'
output = 'plot.png'
title = None

argv = sys.argv[1:]
try:
    opts, args = getopt.getopt(argv, 'i:o:w:h:d:t:', ['help'])
except getopt.GetoptError:
    print('Invalid Arguments.')

for opt, arg in opts:
    if opt == '--help':
        print("py plot_gen.py -i <input/csv> -o <output/png> -w <width/in/int> -h <height/in/int> -d <distance/cm/float> -n <nrc/float> -t <title>")
        sys.exit(0)
    elif opt == '-i':
        incsv = arg_to_file(arg, '.csv', incsv)
    elif opt == '-o':
        output = arg_to_file(arg, '.png', output)
    elif opt == '-w':
        width = str_to_int(arg, width)
    elif opt == '-h':
        height = str_to_int(arg, height)
    elif opt == '-d':
        distance = str_to_float(arg, distance)
    elif opt == '-t':
        title = arg
    else:
        print('Invalid argument: ' + opt + ' ' + arg)


# GET DATA
data = pd.read_csv(incsv)


# CREATE PLOT AND SAVE TO OUTPUT PNG
sns.set_theme(style='ticks')
fig, ax = plt.subplots(figsize=(width, height)) 

sns.stripplot(x="nrc", y="actual", data=data, dodge=False, alpha=0.4, hue='sensor', palette={'hc-sr04':'m','a01a':'b','a01a-h':'g'}, zorder=1)

sns.pointplot(x="nrc", y="actual", data=data, dodge=False, join=True, hue='sensor', palette={'hc-sr04':'purple','a01a':'darkblue','a01a-h':'darkgreen'}, markers="d", scale=0.75, zorder=2) #, ci=None

if title == None:
    title = 'Distance=' + str(distance) + 'cm'
ax.set(title=title, xlabel='NRC', ylabel='Distance (cm)')

ax.axhline(y=distance, color='r', linestyle='-', zorder=-1)
ax.axhline(y=distance+(1+distance*0.003), color='pink', linestyle='--', zorder=-1)
ax.axhline(y=distance-(1+distance*0.003), color='pink', linestyle='--', zorder=-1)
ax.yaxis.grid(True)
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[0:2], labels[0:2], title="Devices", handletextpad=0, loc="lower center", frameon=True)

plt.savefig(output)
