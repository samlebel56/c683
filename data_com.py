import time
import serial
import sys, getopt
from os.path import exists as file_exists
import csv


def str_to_bool(str):
    if str.lower() == 'true' or str == '1':
        return True
    elif str.lower() == 'false' or str == '0':
        return False
    else:
        return False


def str_to_int(str, default = 0):
    if str.isdigit():
        return int(str)
    else:
        return default

def str_to_float(str, default = 0.0):
    try:
        res = float(str)
        return res
    except:
        return default


# SERIAL READ A01A
# This function isn't mine!
# I cannot find where I got it from anymore.
# If you know who/where it's from,
# please let me know, so that I may give proper credit.
def read_a01a(ser):
    buf = bytearray(3)
    idx = 0

    while True:
        c = ser.read(1)[0]
        if idx == 0 and c == 0xFF:
            buf[0] = c
            idx = idx + 1
        elif 0 < idx < 3:
            buf[idx] = c
            idx = idx + 1
        else:
            chksum = sum(buf) & 0xFF
            if chksum == c:
                return (buf[1] << 8) + buf[2]
            idx = 0
    return None


# OPTIONS
num = 100
nrc = 0.03 # 0.03; 0.36; 0.65; 0.95
distance = 30.48 # cm
out_file = 'data.csv'
sensor = 'a01a' # 'a'=a01a; 'h'=a01a-h
serial_port = 'COM3'
is_quiet = True
no_out = False

argv = sys.argv[1:]
try:
    opts, args = getopt.getopt(argv, 'o:s:n:d:c:qah', ['help', 'no-out'])
except getopt.GetoptError:
    print('Invalid Arguments.')
    sys.exit(1)

for opt, arg in opts:
    if opt == '--help':
        print('py data_com.py -o <output/csv> -n <nrc/float> -d <distance/cm/float> -g <angle/int> [-q, -a | -h]')
        sys.exit(0)
    elif opt == '--no-out':
        no_out = True
        is_quiet = False
    elif opt == '-o':
        if len(arg) == 0:
            print('No output given; Will default to "data.csv"')
        elif 0 < len(arg) < 4:
            out_file = arg + '.csv'
        elif len(arg) >= 4 and arg[-4:len(arg)] != '.csv':
            out_file = arg + '.csv'
        else:
            out_file = arg
    elif opt == '-a':
        sensor = 'a01a'
    elif opt == '-h':
        sensor = 'a01a-h'
    elif opt == '-c':
        serial_port = arg
    elif opt == '-n':
        nrc = str_to_float(arg, nrc)
    elif opt == '-q':
        is_quiet = False
    elif opt == '-d':
        distance = str_to_float(arg, distance)
    else:
        print('Invalid argument: ' + opt + ' ' + arg)


# GET DATA
data = []

try:
    serialport = serial.Serial(SERIAL_PORT, 9600, timeout=5)
except:
    print('Failed communicating with serial port COM3.')
    sys.exit(1)

while num > 0:
    d = float(read_a01a(serialport)) / 10.0
    if d == None:
        continue
    else:
        data.append(d)
        if not is_quiet:
            print('%0.2f mm (%0.2f cm)' % (d*10.0, d))
        num = num - 1


# OUTPUT TO CSV FILE
if not no_out:
    f_exists = file_exists(out_file)
    with open(out_file, 'a', newline='') as csvfile:
        fieldnames = ['sensor', 'nrc', 'distance', 'actual']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        if not f_exists:
            writer.writeheader()

        for d in data:
            writer.writerow({'sensor': sensor, 'nrc': nrc, 'distance': distance, 'actual': d})
