import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.patches import PathPatch
import csv
import sys, getopt


def str_to_int(str, default = 0):
    if str.isdigit():
        return int(str)
    else:
        return default


def arg_to_file(name, ext, default):
    if len(name) == 0:
            return default
    elif 0 < len(name) < len(ext):
        return name + ext
    elif len(arg) >= len(ext) and arg[-len(ext):len(arg)] != ext:
        return arg + ext
    else:
        return name


def str_to_float(str, default = 0.0):
    try:
        res = float(str)
        return res
    except:
        return default


# OPTIONS
width = 7 # in
height = 5 # in
# distance = 30.48 # cm # 30.48; 60.96; 91.44
# incsv = ['data/d3048.csv', data]
output = 'plot.png'
title = None
sensor = 'a01a'

argv = sys.argv[1:]
try:
    opts, args = getopt.getopt(argv, 'o:w:h:t:', ['help'])
except getopt.GetoptError:
    print('Invalid Arguments.')

for opt, arg in opts:
    if opt == '--help':
        print("py plot_gen.py -o <output/png> -w <width/in/int> -h <height/in/int> -t <title>")
        sys.exit(0)
    elif opt == '-o':
        output = arg_to_file(arg, '.png', output)
    elif opt == '-w':
        width = str_to_int(arg, width)
    elif opt == '-h':
        height = str_to_int(arg, height)
    elif opt == '-t':
        title = arg
    else:
        print('Invalid argument: ' + opt + ' ' + arg)

# GET DATA
input_csv = ("data/d3048.csv","data/d6096.csv","data/d9144.csv")

data = pd.DataFrame({'sensor': ['a01a','a01a','a01a','a01a','a01a-h','a01a-h','a01a-h','a01a-h'], 'nrc': [0.03, 0.36, 0.65, 0.95, 0.03, 0.36, 0.65, 0.95], 'mean': [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]})
# print(data)

for data_csv in input_csv:
    df = pd.read_csv(data_csv)
    for sensor in ('a01a', 'a01a-h'):
        for nrc in (0.03, 0.36, 0.65, 0.95):
            temp = (df[(df['sensor'] == sensor) & (df['nrc'] == nrc)])[['distance','actual']].apply(lambda x: x['distance'] - x['actual'], axis=1)
            data.loc[((data['sensor'] == sensor) & (data['nrc'] == nrc), 'mean')] += temp.sum()

data['mean'] = data['mean'].map(lambda m: -(m / 300.0))

# print(data)


# CREATE PLOT AND SAVE TO OUTPUT PNG
sns.set_theme(style='ticks')
fig, ax = plt.subplots(figsize=(width, height)) 

sns.pointplot(x="nrc", y="mean", data=data, dodge=False, join=True, hue='sensor', palette={'a01a':'darkblue','a01a-h':'darkgreen'}, markers="d", scale=0.75, zorder=2, ci=None)

if title == None:
    title = 'Average Difference Across All Distances'
ax.set(title=title, xlabel='NRC', ylabel='Average Difference (cm)')
ax.yaxis.grid(True)
ax.axhline(y=0, color='grey', linestyle='-', zorder=-1)

plt.savefig(output)

