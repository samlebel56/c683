# C683 - Natural Science Experiment

This is code used during my experiment on the relation between NRC of a target and the accuracy of ultrasonic rangefinder.

## Usage
All code was testing on Windows only, and requires Python 3.8 to run.

```shell
py data_com.py -o <output/csv> -n <nrc/float> -d <distance/cm/float> -g <angle/int> -q <quiet/bool> [-a | -h]
```

```shell
py plot_gen.py -i <input/csv> -o <output/png> -w <width/in/int> -h <height/in/int> -d <distance/cm/float> -n <nrc/float> -t <title>
```

```shell
py avg_plot_gen.py -o <output/png> -w <width/in/int> -h <height/in/int> -t <title>
```
